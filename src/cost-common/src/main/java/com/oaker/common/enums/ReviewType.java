package com.oaker.common.enums;

public enum ReviewType {
    PASS,
    REJECT
}
