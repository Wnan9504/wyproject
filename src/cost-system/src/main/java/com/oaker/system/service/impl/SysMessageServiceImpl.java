package com.oaker.system.service.impl;

import java.util.List;

import com.oaker.common.utils.DateUtils;
import com.oaker.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.oaker.system.mapper.SysMessageMapper;
import com.oaker.system.domain.SysMessage;
import com.oaker.system.service.ISysMessageService;

/**
 * 消息Service业务层处理
 * 
 * @author ruoyi
 * date 2023-11-23
 */
@Service
public class SysMessageServiceImpl implements ISysMessageService 
{
    @Autowired
    private SysMessageMapper sysMessageMapper;


    /**
     * 查询消息
     * 
     * @param messageId 消息主键
     * @return 消息
     */
    @Override
    public SysMessage selectSysMessageByMessageId(Integer messageId)
    {
        SysMessage sysMessage = sysMessageMapper.selectSysMessageByMessageId(messageId);
        // sysMessage.setStatus("1");

        // this.updateSysMessage(sysMessage);
        return sysMessageMapper.selectSysMessageByMessageId(messageId);
    }

    /**
     * 查询我的消息列表
     * 
     * @param sysMessage 消息
     * @return 消息
     */
    @Override
    public List<SysMessage> selectSysMessageList(SysMessage sysMessage)
    {
        Long toUserId = SecurityUtils.getUserId();
        sysMessage.setReceiveUserid(toUserId);

        //System.out.println("sms:"+sysMessage);
        return sysMessageMapper.selectMySysMessageList(sysMessage);
    }

    /**
     * 新增消息
     * 
     * @param sysMessage 消息
     * @return 结果
     */
    @Override
    public int insertSysMessage(SysMessage sysMessage)
    {
        sysMessage.setCreateTime(DateUtils.getNowDate());
        return sysMessageMapper.insertSysMessage(sysMessage);
    }




    @Override
    public boolean insertHourReviewMessage(SysMessage sysMessage)
    {
//        SysMessage msg = new SysMessage();
//        msg.setTitle("审核");
//        msg.setReceiveUserid(userid);
//        msg.setMessageContent("你的id为"+ source+ "的日志已被审核"+mesg);
//        msg.setSourceId(source);
//        msg.setCreateTime(DateUtils.getNowDate());

        sysMessageMapper.insertSysMessage(sysMessage);
        return true;
    }

    /**
     * 修改消息
     * 
     * @param sysMessage 消息
     * @return 结果
     */
    @Override
    public int updateSysMessage(SysMessage sysMessage)
    {
        sysMessage.setUpdateTime(DateUtils.getNowDate());
        return sysMessageMapper.updateSysMessage(sysMessage);
    }

    /**
     * 批量删除消息
     * 
     * @param messageIds 需要删除的消息主键
     * @return 结果
     */
    @Override
    public int deleteSysMessageByMessageIds(Integer[] messageIds)
    {
        return sysMessageMapper.deleteSysMessageByMessageIds(messageIds);
    }

    /**
     * 删除消息信息
     * 
     * @param messageId 消息主键
     * @return 结果
     */
    @Override
    public int deleteSysMessageByMessageId(Integer messageId)
    {
        return sysMessageMapper.deleteSysMessageByMessageId(messageId);
    }


    /**
     * 批量设置消息为已读
     *
     * @param messageIds 需要设置已读的消息主键
     * @return 结果
     */
    @Override
    public int readSysMessageByMessageIds(Integer[] messageIds)
    {
        return sysMessageMapper.readSysMessageByMessageIds(messageIds);
    }


}
