package com.oaker.system.service;

import java.util.List;
import com.oaker.system.domain.SysMessage;

/**
 * 消息Service接口
 * 
 * @author ruoyi
 * @date 2023-11-23
 */
public interface ISysMessageService 
{
    /**
     * 查询消息
     * 
     * @param messageId 消息主键
     * @return 消息
     */
    public SysMessage selectSysMessageByMessageId(Integer messageId);

    /**
     * 查询消息列表
     * 
     * @param sysMessage 消息
     * @return 消息集合
     */
    public List<SysMessage> selectSysMessageList(SysMessage sysMessage);

    /**
     * 新增消息
     * 
     * @param sysMessage 消息
     * @return 结果
     */
    public int insertSysMessage(SysMessage sysMessage);


    boolean insertHourReviewMessage(SysMessage sysMessage);

    /**
     * 修改消息
     * 
     * @param sysMessage 消息
     * @return 结果
     */
    public int updateSysMessage(SysMessage sysMessage);

    /**
     * 批量删除消息
     * 
     * @param messageIds 需要删除的消息主键集合
     * @return 结果
     */
    public int deleteSysMessageByMessageIds(Integer[] messageIds);

    /**
     * 设置消息为已读
     * 
     * @param
     * @return 结果
     */
    public int deleteSysMessageByMessageId(Integer messageId);

    public int readSysMessageByMessageIds(Integer[] messageIds);

}
